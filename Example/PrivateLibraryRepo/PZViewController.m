//
//  PZViewController.m
//  PrivateLibraryRepo
//
//  Created by feimengchang on 05/27/2017.
//  Copyright (c) 2017 feimengchang. All rights reserved.
//

#import "PZViewController.h"
#import "Foo.h"

@interface PZViewController ()

@end

@implementation PZViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    Foo *f = [Foo new];
    [f foo];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
