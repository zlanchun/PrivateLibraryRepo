# PrivateLibraryRepo

[![CI Status](http://img.shields.io/travis/feimengchang/PrivateLibraryRepo.svg?style=flat)](https://travis-ci.org/feimengchang/PrivateLibraryRepo)
[![Version](https://img.shields.io/cocoapods/v/PrivateLibraryRepo.svg?style=flat)](http://cocoapods.org/pods/PrivateLibraryRepo)
[![License](https://img.shields.io/cocoapods/l/PrivateLibraryRepo.svg?style=flat)](http://cocoapods.org/pods/PrivateLibraryRepo)
[![Platform](https://img.shields.io/cocoapods/p/PrivateLibraryRepo.svg?style=flat)](http://cocoapods.org/pods/PrivateLibraryRepo)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PrivateLibraryRepo is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PrivateLibraryRepo"
```

## Author

feimengchang, zlanchun@icloud.com

## License

PrivateLibraryRepo is available under the MIT license. See the LICENSE file for more info.
