#
# Be sure to run `pod lib lint PrivateLibraryRepo.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PrivateLibraryRepo'
  #版本号，默认从0.1.0开始
  s.version          = '0.1.0'
  s.summary          = 'A short description of PrivateLibraryRepo.'

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC
  s.homepage         = 'https://gitlab.com/zlanchun/PrivateLibraryRepo'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'feimengchang' => 'zlanchun@gmail.com' }
  #source 地址，当前私有库的git地址                                                                
  s.source           = { 
                        :git => 'https://zlanchun@gitlab.com/zlanchun/PrivateLibraryRepo.git', 
                        :tag => s.version.to_s 
                       }

  s.ios.deployment_target = '7.0'
  s.platform     = :ios, '7.0'
  s.source_files = 'PrivateLibraryRepo/Classes/**/*'
  
end
