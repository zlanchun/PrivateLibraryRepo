//
//  PZAppDelegate.h
//  PrivateLibraryRepo
//
//  Created by feimengchang on 05/27/2017.
//  Copyright (c) 2017 feimengchang. All rights reserved.
//

@import UIKit;

@interface PZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
